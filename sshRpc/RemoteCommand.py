try:
    import paramiko

except ImportError:
    print("Trying to Install 'paramiko' module for ssh connection\n")
    import os
    os.system('py -m pip install paramiko')
    import paramiko

class RemoteCommand():
    
    def __init__(self, hostConfig):

        self.connection = self.getConnection(hostConfig)
        

    def display_result(self, bytesObject):
        '''Displays command output with line breakes'''
        for line in bytesObject.decode('utf-8').split('\n'):
            print(line)

    def getConnection(self, hostConfig):
    
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
        if(hostConfig['auth'] == 'key'):
            key = paramiko.RSAKey.from_private_key_file(hostConfig['key']['path'], password=hostConfig['key']['pass'])
            ssh.connect(hostConfig['address'], username=hostConfig['user'], pkey=key)
        
            return ssh
        
        ssh.connect(hostConfig['address'], username=hostConfig['user'], password=hostConfig['password'])
    
        return ssh

    def runCommands(self, commands):
            
        print('Send command list: %s' % commands)
        for command in commands:
            print("Executing {}".format(command))
            stdIOs = self.connection.exec_command(command)
            
            for io in stdIOs:
                if(io.readable()):
                    bytesObject = io.read()
                    if(bytesObject):
                        self.display_result(bytesObject)
            
    def closeConnection(self):
        self.connection.close()
