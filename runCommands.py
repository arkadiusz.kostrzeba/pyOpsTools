import sys
from sshRpc.RemoteCommand import RemoteCommand
from config import HOSTS

if(len(sys.argv) > 1 ):
    hostId = sys.argv[1]

else:
    raise ValueError('Missing host id in argument list at position 1.')
	
	
if(len(sys.argv) > 2):
    config = HOSTS[sys.argv[1]]
    remoteHost = RemoteCommand(config)
    remoteHost.runCommands(sys.argv[2:])
    remoteHost.closeConnection()

else:
    raise ValueError('Missing command list at possition 2')